FROM adoptopenjdk/openjdk11
VOLUME /tmp
ADD /target/cookit-login-1.0.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
