package com.cookit.service;

import com.cookit.dto.UserDto;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author dparedes
 */
public interface UserService {

    public ResponseEntity login(UserDto userDto);
    public boolean authenticate(UserDto userDto);
    public String getJwtKey(String username);
    public boolean isPayloadValid(UserDto userDto);

}
