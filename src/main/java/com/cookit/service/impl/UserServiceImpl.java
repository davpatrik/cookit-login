package com.cookit.service.impl;

import com.cookit.dto.UserDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;
import com.cookit.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author dparedes
 */
@Service
public class UserServiceImpl implements UserService {

    private final Integer JWT_TOKEN_EXPIRATION_TIME = 3 * 3600 * 1000; // 3 hours
    
    @Override
    public ResponseEntity login(@Validated @RequestBody UserDto userDto) {
        //System.out.println("userDto: " + userDto);
        if (!isPayloadValid(userDto)) {
            userDto = new UserDto();
            userDto.setLog("Username and password required");
            return new ResponseEntity<>(userDto, HttpStatus.BAD_REQUEST);
        } else {
            if (!authenticate(userDto)) {
                userDto.setLog("Invalid credentials");
                return new ResponseEntity<>(userDto, HttpStatus.FORBIDDEN);
            } else {
                String username = userDto.getUser();
                String token = getJwtKey(userDto.getUser());
                userDto.setToken(token);
                userDto.setPassword(null);
                userDto.setLog("Success, welcome to CookIt!");
                return new ResponseEntity<>(userDto, HttpStatus.OK);
            }
        }
    }
    
    @Override
    public boolean authenticate(UserDto userDto) {
        // TODO: user and password validation in DB, by default return true if user and password are not empty
        return !userDto.getUser().isEmpty() && !userDto.getPassword().isEmpty();
    }
    
    @Override
    public String getJwtKey(String username) {
        String secretKey = "2f5ae96c-b558-4c7b-a590-a501ae1c3f6c";        
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                //.setId("DevOpsApp")
                .setId("CookItApp")
                .setSubject(username)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))                
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_EXPIRATION_TIME))                
                .signWith(SignatureAlgorithm.HS256,
                        secretKey.getBytes()).compact();

        return "JwtKey " + token;
    }

    @Override
    public boolean isPayloadValid(UserDto userDto) {
        return userDto != null && userDto.getUser() != null && userDto.getPassword() != null;
    }
    
}
