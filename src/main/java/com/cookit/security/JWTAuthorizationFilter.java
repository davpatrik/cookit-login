package com.cookit.security;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;

/**
 *
 * @author dparedes
 */
public class JWTAuthorizationFilter extends OncePerRequestFilter {

    private final String HEADER_API_KEY = "API-KEY";
    private final String HEADER_JWT_KEY = "JWT-KEY";
    private static final String PREFIX = "JwtKey ";    
    private final String API_KEY = "2f5ae96c-b558-4c7b-a590-a501ae1c3f6c";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        //System.out.println("doFilterInternal");
        try {
            if (existeJWTToken(request, response) && validateApiKey(request)) {
                Claims claims = validateJwtKey(request);
                if (claims.get("authorities") != null) {
                    setUpSpringAuthentication(claims);
                } else {
                    SecurityContextHolder.clearContext();
                }
            } else {
                SecurityContextHolder.clearContext();
            }
            chain.doFilter(request, response);
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException e) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
            return;
        }
    }

    private Claims validateJwtKey(HttpServletRequest request) {        
        String jwtToken = request.getHeader(HEADER_JWT_KEY).replace(PREFIX, "");
        System.out.println("jwtToken: " + jwtToken);
        return Jwts.parser().setSigningKey(API_KEY.getBytes()).parseClaimsJws(jwtToken).getBody();
    }

    private boolean validateApiKey(HttpServletRequest request){
        String apiKey = request.getHeader(HEADER_API_KEY);
        System.out.println("apiKey: " + apiKey);
        return apiKey.equals(API_KEY);
    }
    
    /**
     * Metodo para autenticarnos dentro del flujo de Spring
     *
     * @param claims
     */
    private void setUpSpringAuthentication(Claims claims) {
        @SuppressWarnings("unchecked")
        List<String> authorities = (List) claims.get("authorities");

        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(claims.getSubject(), null,
                authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
        SecurityContextHolder.getContext().setAuthentication(auth);

    }

    private boolean existeJWTToken(HttpServletRequest request, HttpServletResponse res) {
        String authenticationHeader = request.getHeader(HEADER_JWT_KEY);
        if (authenticationHeader == null || !authenticationHeader.startsWith(PREFIX)) {
            return false;
        }
        return true;
    }

    public String getPREFIX() {
        return PREFIX;
    }

}
