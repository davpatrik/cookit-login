package com.cookit.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author dparedes
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserDto {
    
    private String user;
    private String password;
    private String token;    
    private String log;

    public UserDto(String user, String password) {
        this.user = user;
        this.password = password;
    }
    
}
