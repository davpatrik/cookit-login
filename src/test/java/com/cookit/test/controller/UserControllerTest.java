package com.cookit.test.controller;

import com.cookit.controller.UserController;
import com.cookit.dto.UserDto;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author dparedes
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserControllerTest {

    @Autowired
    private UserController userController;

    @Test
    public void contextLoads() throws Exception {
        System.out.println("TEST userController contextLoads");
        org.assertj.core.api.Assertions.assertThat(userController).isNotNull();
    }

    @Test
    public void whenRequestJwtToken_thenReturnCorrectRequest() {
        System.out.println("TEST whenRequestJwtToken_thenReturnCorrectRequest");
        try {
            UserDto userDtoRequest = new UserDto("user", "password");
            ResponseEntity<UserDto> response = userController.login(userDtoRequest);
            System.out.println("response: " + response);
            Assert.assertNotNull(response.getBody().getToken());
        } catch (Exception ex) {            
            Logger.getLogger(UserControllerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void whenBadRequest_thenReturnNoToken() {
        System.out.println("TEST whenBadRequest_thenReturnNoToken");
        try {
            UserDto userDtoRequest = new UserDto("user", "");
            ResponseEntity<UserDto> response = userController.login(userDtoRequest);
            System.out.println("response: " + response);            
            Assert.assertNull(response.getBody() == null ? null : response.getBody().getToken());            
        } catch (Exception ex) {            
            Logger.getLogger(UserControllerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
