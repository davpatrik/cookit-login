# Project description

## "cookit-login"
It is a spring boot microservice project (SpringBoot v2.4.1 + Maven POM 4.0.0 + JWT + JUnit), which provides REST service to login and obtain JWT credentials to be used in "routes" microservice.

## CI/CD configurations
cookit-login contains file [.gitlab-ci.yml] which is the pipeline configuration file for GitLab, it contains the following stages:
- build
- test: Run tests for ensure code quality
- package: Build and push docker image into [DockerHub repository](https://hub.docker.com/repository/docker/davpatrik/cookit-login)
- deploy: Deploy the image form [DockerHub repository](https://hub.docker.com/repository/docker/davpatrik/cookit-login) into a Kubernetes Cluster configured from scratch in DigitalOcean Cloud Services. It depends of file '/kubernetes/deployment.yml'

## Software requirements
It is required a previous installation of:
- openjdk 11: [Instalación guide](https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/).
- Git: [Instalación guide](https://github.com/git-guides/install-git#:~:text=To%20install%20Git%2C%20navigate%20to,installation%20by%20typing%3A%20git%20version%20.).
- Maven: [Instalación guide](https://www.mkyong.com/maven/how-to-install-maven-in-windows/).
- Docker: [Instalación guide](https://runnable.com/docker/install-docker-on-linux).

# Project download and execution

## Docker image pull
In order to download the docker image, execute the following command:

```bash
docker pull davpatrik/cookit-login:latest
```

## Docker image run
In order to run the docker image, execute the following command:

```bash
docker run -p 8080:8080 davpatrik/cookit-login:latest
```

## Download cookit-login from Git repository
Download sources at GitHub [https://gitlab.com/davpatrik/cookit-login.git](https://gitlab.com/davpatrik/cookit-login.git). 

## Running cookit-login
Run project in IDE: 
-In Eclipse: selectProject->RunAs->3 Spring Boot App, or 
-In NetBeans: selectProject->buildWhitDependencies then selectProject->Run 
Or in command line, in CMD first go to main project folder "..\cookit-login" and execute the following command:
```bash
mvn spring-boot:run
```
# Project testing

## Get jwtToken form cookit-login
After project is running, try GET in the next URL in postman-> http://localhost:8080/user/login, send the jSon:
```bash
{
    "user": "username",
    "password": "password"
} 
```

The result must be:
```bash
{
    "user": "asdsdf",
    "password": null,
    "token": "JwtKey eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJDb29rSXRBcHAiLCJzdWIiOiJhc2RzZGYiLCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwiaWF0IjoxNjM1NTU5NTYwLCJleHAiOjE2MzU1NjA1NjB9.KPg9r4rNAFds9E24T0fU115Rpnooqc2Lcffl4ykAwEU",
    "log": "Success, welcome to CookIt!"
}
```

## Use jwtToken + apiKey to consume principal REST service cookit-routes project once running
Try GET in the next URL in postman-> http://localhost:8081/routes, send the next params

### Header params
```bash
Content-Type: application/json
API-KEY: 2f5ae96c-b558-4c7b-a590-a501ae1c3f6c
JWT-KEY: $token
```
### Body params
```bash
{
    "item-ids": [1, 2, 3, 4, 9999]
}
```

Or simply try CURL
```bash
curl -X POST \
-H "API-KEY: 2f5ae96c-b558-4c7b-a590-a501ae1c3f6c" \
-H "JWT-KEY: $token" \
-H "Content-Type: application/json" \
-d '{ "item-ids": [1, 2, 3, 4, 9999] }' \
http://localhost:8081/routes
```

The result should be:
```bash
{
  "picks": ["Z2", "Y8"],
  "out-of-stock": ["Z2"]
}
```
## //TODO
- Add SONAR dependencies for static code revision.
- Enable Kubernetes Cluster on GitLab configuration in order to enable CD (continuous delivery).